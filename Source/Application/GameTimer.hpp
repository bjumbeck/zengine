#pragma once

class GameTimer
{
    public:
        GameTimer();

        // Time in Seconds
        float TotalTime() const;
        float DeltaTime() const;

        void Reset();
        void Start();
        void Stop();
        void Tick();

    private:
        double secondsPerFrame;
        double deltaTime;

        __int64 baseTime;
        __int64 pausedTime;
        __int64 stopTime;
        __int64 prevTime;
        __int64 currTime;

        bool stopped;
};

