#include <Windows.h>
#include "GameTimer.hpp"

GameTimer::GameTimer()
    : secondsPerFrame(0.0)
      , deltaTime(-1.0)
      , baseTime(0)
      , pausedTime(0)
      , stopTime(0)
      , prevTime(0)
      , currTime(0)
      , stopped(false)
{
    __int64 countsPerSecond;
    QueryPerformanceFrequency(reinterpret_cast<LARGE_INTEGER*>(&countsPerSecond));
    secondsPerFrame = 1.0 / static_cast<double>(countsPerSecond);
}

float GameTimer::TotalTime() const
{
    // If we are stopped, do not count the time that has passed since we stopped.
    // Moreover, if we previously already had a pause, the distance 
    // mStopTime - mBaseTime includes paused time, which we do not want to count.
    // To correct this, we can subtract the paused time from mStopTime:  
    if (stopped)
    {
        return static_cast<float>((stopTime - pausedTime - baseTime) * secondsPerFrame);
    }
    // The distance mCurrTime - mBaseTime includes paused time,
    // which we do not want to count.  To correct this, we can subtract 
    // the paused time from mCurrTime:  
    else
    {
        return static_cast<float>((currTime - pausedTime - baseTime) * secondsPerFrame);
    }
}

float GameTimer::DeltaTime() const
{
    return static_cast<float>(deltaTime);
}

void GameTimer::Reset()
{
    __int64 tempCurrent;
    QueryPerformanceCounter(reinterpret_cast<LARGE_INTEGER*>(&tempCurrent));

    baseTime = tempCurrent;
    prevTime = tempCurrent;
    stopTime = 0;
    stopped = false;
}

void GameTimer::Start()
{
    __int64 startTime;
    QueryPerformanceCounter(reinterpret_cast<LARGE_INTEGER*>(&startTime));

    // Accumulate the time elapsed between stop and start pairs
    if (stopped)
    {
        pausedTime += startTime - stopTime;

        prevTime = startTime;
        stopTime = 0;
        stopped = false;
    }
}

void GameTimer::Stop()
{
    if (!stopped)
    {
        __int64 tempCurrent;
        QueryPerformanceCounter(reinterpret_cast<LARGE_INTEGER*>(&tempCurrent));

        stopTime = tempCurrent;
        stopped = true;
    }
}

void GameTimer::Tick()
{
    if (stopped)
    {
        deltaTime = 0.0;
        return;
    }

    __int64 currentTime;
    QueryPerformanceCounter(reinterpret_cast<LARGE_INTEGER*>(&currentTime));
    currTime = currentTime;

    // Time difference between this frame and the previous
    deltaTime = (currTime - prevTime) * secondsPerFrame;

    // Prepare for the next frame
    prevTime = currTime;

    // Need this to force nonnegative DT. If the processor goes
    // into a power save mode or we get shuffled to another processor
    // then DT can be negative.
    if (deltaTime < 0.0)
    {
        deltaTime = 0.0;
    }
}

