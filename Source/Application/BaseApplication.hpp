#pragma once

#include <string>
#include <Windows.h>
#include <d3d11.h>
#include "GameTimer.hpp"
#include <memory>
#include <GameView/GameViewInterfaces.hpp>
#include <GameLogic/BaseGameWorld.hpp>

class BaseApplication
{
    public:
        explicit BaseApplication();
        virtual ~BaseApplication();

        // Accessors
        float GetAspectRatio() const { return static_cast<float>(clientWidth / clientHeight); }
        HWND GetMainWindow() const { return mainHwnd; }
        HINSTANCE GetAppInstance() const { return appInstance; }
        IRenderer* GetRenderer() const { return renderer.get(); }
        BaseGameWorld* GetGameWorld() const { return gameWorld.get(); }
        const GameTimer& GetGameTimer() const { return gameTimer; }

        // Base Functionality
        int RunApp();

        virtual HRESULT InitializeWin32(HINSTANCE hInstance);
        virtual HRESULT InitializeD3D();
        virtual HRESULT InitGameWorld();

        virtual void UpdateScene(float deltaTime);
        // Normally you wouldn't need a deltaTime for rendering, but in this case we need to pass it to the game view for UI animations, etc.
        virtual void DrawScene(float deltaTime);

        virtual LRESULT MsgProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);

        // Callbacks
        virtual void OnResize();

    private:
        static LRESULT CALLBACK InternalMsgProc(HWND hwnd, int msg, WPARAM wParam, LPARAM lParam);

    protected:
        // Generic Stuff
        GameTimer gameTimer;

        // DirectX Stuff
        ID3D11Device* d3dDevice;
        ID3D11DeviceContext* d3dImmediateContext;
        IDXGISwapChain* swapChain;
        ID3D11Texture2D* depthStencilBuffer;
        ID3D11RenderTargetView* renderTargetView;
        ID3D11DepthStencilView* depthStencilView;
        D3D11_VIEWPORT screenViewport;

        // Win32
        HINSTANCE appInstance;
        HWND mainHwnd;

        // Renderer
        // TODO: Define the renderer in ctor when implemented
        std::unique_ptr<IRenderer> renderer;

        // Game Stuff
        std::unique_ptr<BaseGameWorld> gameWorld;

        // Application Stuff
        bool appPaused;
        bool appMinimized;
        bool appMaximized;
        bool appResizing;

        int clientWidth;
        int clientHeight;
};

extern std::unique_ptr<BaseApplication> g_Application;