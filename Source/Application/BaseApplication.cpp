#include "BaseApplication.hpp"
#include <ProjectGlobals.hpp>
#include <cassert>
#include <GameLogic/BaseGameWorld.hpp>
#include <GameView/HumanGameView.hpp>

extern std::unique_ptr<BaseApplication> g_Application(nullptr);

BaseApplication::BaseApplication()
      : d3dDevice(nullptr)
      , d3dImmediateContext(nullptr)
      , swapChain(nullptr)
      , depthStencilBuffer(nullptr)
      , renderTargetView(nullptr)
      , depthStencilView(nullptr)
      , appInstance(nullptr)
      , mainHwnd(nullptr)
      , renderer(nullptr) // TODO: Change to our renderer implementation later.
      , gameWorld(new BaseGameWorld())
      , appPaused(false)
      , appMinimized(false)
      , appMaximized(false)
      , appResizing(false)
      , clientWidth(800)
      , clientHeight(600)
{
    ZeroMemory(&screenViewport, sizeof(D3D11_VIEWPORT));

    // Test adding a single human view
    gameWorld->AddView(std::make_shared<HumanGameView>(nullptr));

    g_Application.reset(this);
}

BaseApplication::~BaseApplication()
{
    // Release COM Objects
    ReleaseCOM(renderTargetView);
    ReleaseCOM(depthStencilView);
    ReleaseCOM(swapChain);
    ReleaseCOM(depthStencilBuffer);

    // Restore Default settings
    if (d3dImmediateContext)
        d3dImmediateContext->ClearState();

    ReleaseCOM(d3dImmediateContext);
    ReleaseCOM(d3dDevice);

    g_Application.release();
}

int BaseApplication::RunApp()
{
    MSG message = {nullptr};

    gameTimer.Reset();

    while (message.message != WM_QUIT)
    {
        if (PeekMessage(&message, nullptr, 0, 0, PM_REMOVE))
        {
            TranslateMessage(&message);
            DispatchMessage(&message);
        }
        else
        {
            gameTimer.Tick();

            if (!appPaused)
            {
                UpdateScene(gameTimer.DeltaTime());
                DrawScene(gameTimer.DeltaTime());
            }
            else
            {
                Sleep(100);
            }
        }
    }

    return static_cast<int>(message.wParam);
}

HRESULT BaseApplication::InitializeWin32(HINSTANCE hInstance)
{
    appInstance = hInstance;

    WNDCLASS wc = { 0 };
    wc.style = CS_HREDRAW | CS_VREDRAW;
    wc.lpfnWndProc = reinterpret_cast<WNDPROC>(InternalMsgProc);
    wc.cbClsExtra = 0;
    wc.cbWndExtra = 0;
    wc.hInstance = appInstance;
    wc.hIcon = LoadIcon(nullptr, IDI_APPLICATION);
    wc.hCursor = LoadCursor(nullptr, IDC_ARROW);
    wc.hbrBackground = static_cast<HBRUSH>(GetStockObject(NULL_BRUSH));
    wc.lpszMenuName = nullptr;
    wc.lpszClassName = L"D3DWndClassName";

    if (!RegisterClass(&wc))
    {
        MessageBox(nullptr, L"RegisterClass Failed.", nullptr, 0);
        return E_FAIL;
    }

    RECT clientRect = { 0, 0, clientWidth, clientHeight };
    AdjustWindowRect(&clientRect, WS_OVERLAPPEDWINDOW, false);
    int width = clientRect.right - clientRect.left;
    int height = clientRect.bottom - clientRect.top;

    mainHwnd = CreateWindow(L"D3DWndClassName",
                            L"ZEngine",
                            WS_OVERLAPPEDWINDOW,
                            CW_USEDEFAULT,
                            CW_USEDEFAULT,
                            width,
                            height,
                            nullptr,
                            nullptr,
                            appInstance,
                            nullptr);
    if (!mainHwnd)
    {
        MessageBox(nullptr, L"CreateWindow Failed.", nullptr, 0);
        return E_FAIL;
    }

    SetWindowLongPtr(mainHwnd, GWLP_USERDATA, reinterpret_cast<LONG>(this));

    ShowWindow(mainHwnd, SW_SHOW); // TODO: Let the user of the class do this instead?
    UpdateWindow(mainHwnd);

    return S_OK;
}

HRESULT BaseApplication::InitializeD3D()
{
    UINT createDeviceFalgs = 0;
    #if defined(DEBUG) || defined(_DEBUG)
        createDeviceFalgs |= D3D11_CREATE_DEVICE_DEBUG;
    #endif

    D3D_FEATURE_LEVEL featureLevel;
    HRESULT hr = D3D11CreateDevice(nullptr,
                                   D3D_DRIVER_TYPE_HARDWARE,
                                   nullptr,
                                   createDeviceFalgs,
                                   nullptr,
                                   0,
                                   D3D11_SDK_VERSION,
                                   &d3dDevice,
                                   &featureLevel,
                                   &d3dImmediateContext);
    if (FAILED(hr))
    {
        MessageBox(nullptr, L"D3D11CreateDevice Failed.", nullptr, 0);
        return hr;
    }

    // Swap Chain
    DXGI_SWAP_CHAIN_DESC sd;
    sd.BufferDesc.Width = clientWidth;
    sd.BufferDesc.Height = clientHeight;
    sd.BufferDesc.RefreshRate.Numerator = 60;
    sd.BufferDesc.RefreshRate.Denominator = 1;
    sd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
    sd.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
    sd.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
    sd.SampleDesc.Count = 1;    // No 4X MSAA
    sd.SampleDesc.Quality = 0;  // No 4X MSAA

    sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
    sd.BufferCount = 1;
    sd.OutputWindow = mainHwnd;
    sd.Windowed = true;
    sd.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
    sd.Flags = 0;

    IDXGIDevice* dxgiDevice = nullptr;
    HR_DEBUG(d3dDevice->QueryInterface(__uuidof(IDXGIDevice), reinterpret_cast<void**>(&dxgiDevice)));

    IDXGIAdapter* dxgiAdapter = nullptr;
    HR_DEBUG(dxgiDevice->GetParent(__uuidof(IDXGIAdapter), reinterpret_cast<void**>(&dxgiAdapter)));

    IDXGIFactory* dxgiFactory = nullptr;
    HR_DEBUG(dxgiAdapter->GetParent(__uuidof(IDXGIFactory), reinterpret_cast<void**>(&dxgiFactory)));
    HR_DEBUG(dxgiFactory->CreateSwapChain(d3dDevice, &sd, &swapChain));

    ReleaseCOM(dxgiDevice);
    ReleaseCOM(dxgiAdapter);
    ReleaseCOM(dxgiFactory);

    OnResize();

    return S_OK;
}

HRESULT BaseApplication::InitGameWorld()
{
    // Define gameWorld to a concrete class
    // Inititialize that class

    // Add views to that game world.

    return S_OK;
}

void BaseApplication::UpdateScene(float deltaTime)
{
    if (gameWorld)
    {
        gameWorld->OnUpdate(deltaTime);
    }
}

void BaseApplication::DrawScene(float deltaTime)
{
    // TODO: GameWorld should take care of the rendering of the views itself.
    if (gameWorld)
    {
        for (const auto view : gameWorld->GetGameViews())
        {
            view->OnRender(deltaTime);
        }
    }
}

LRESULT BaseApplication::MsgProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    switch (msg)
    {
        case WM_KEYUP:
        case WM_CHAR:
        case WM_MOUSEMOVE:
        case WM_LBUTTONDOWN:
        case WM_LBUTTONUP:
        case WM_RBUTTONDOWN:
        case WM_RBUTTONUP:
        {
            if (g_Application->GetGameWorld())
            {
                BaseGameWorld* game = g_Application->GetGameWorld();

                if (game)
                {
                    AppMsg tempMessage;
                    tempMessage.hwnd = hwnd;
                    tempMessage.msg = msg;
                    tempMessage.wParam = wParam;
                    tempMessage.lParam = lParam;
                    auto gameViews = game->GetGameViews();
                    for (auto rIter = gameViews.rbegin(); rIter != gameViews.rend(); ++rIter)
                    {
                        if ((*rIter)->OnMsgProc(tempMessage))
                        {
                            // Break out of for loop if OnMsgProc returns true which means it ate the message.
                            break;
                        }
                    }
                }
                else
                {
                    MessageBox(nullptr, L"GameWorld is null", nullptr, 0);
                }
            }
        }
        break;

        // WM_DESTROY is sent when the window is being destroyed.
        case WM_DESTROY:
            PostQuitMessage(0);
            return 0;

        case WM_ACTIVATE:
            if (LOWORD(wParam) == WA_INACTIVE)
            {
                appPaused = true;
                gameTimer.Stop();
            }
            else
            {
                appPaused = false;
                gameTimer.Start();
            }
            return 0;

        case WM_SIZE:
            clientWidth = LOWORD(lParam);
            clientHeight = HIWORD(lParam);
            if (d3dDevice)
            {
                if (wParam == SIZE_MINIMIZED)
                {
                    appPaused = true;
                    appMinimized = true;
                    appMaximized = false;
                }
                else if (wParam == SIZE_MAXIMIZED)
                {
                    appPaused = false;
                    appMinimized = false;
                    appMaximized = true;
                    OnResize();
                }
                else if (wParam == SIZE_RESTORED)
                {
                    if (appMinimized)
                    {
                        appPaused = false;
                        appMinimized = false;
                        OnResize();
                    }
                    else if (appMaximized)
                    {
                        appPaused = false;
                        appMaximized = false;
                        OnResize();
                    }
                    else if (appResizing)
                    {
                        
                    }
                    else
                    {
                        OnResize();
                    }
                }
            }
            return 0;

        case WM_ENTERSIZEMOVE:
            appPaused = true;
            appResizing = true;
            gameTimer.Stop();
            return 0;

        case WM_EXITSIZEMOVE:
            appPaused = false;
            appResizing = false;
            gameTimer.Start();
            OnResize();
            return 0;

        case WM_MENUCHAR:
            return MAKELRESULT(0, MNC_CLOSE);

        case WM_GETMINMAXINFO:
            reinterpret_cast<MINMAXINFO*>(lParam)->ptMinTrackSize.x = 200;
            reinterpret_cast<MINMAXINFO*>(lParam)->ptMinTrackSize.y = 200;
            return 0;
    }

    return DefWindowProc(hwnd, msg, wParam, lParam);
}

void BaseApplication::OnResize()
{
    assert(d3dImmediateContext);
    assert(d3dDevice);
    assert(swapChain);

    ReleaseCOM(renderTargetView);
    ReleaseCOM(depthStencilView);
    ReleaseCOM(depthStencilBuffer);

    // Clear GameViews
    // TODO: Is Resizing really a lost device? Also what about other times where we lose the device, this isn't being handled.
    if (gameWorld)
    {
        for (const auto& view : gameWorld->GetGameViews())
        {
            view->OnLostDevice();
        }
    }

    // Resize swap chain and recreate target view
    HR_DEBUG(swapChain->ResizeBuffers(1, clientWidth, clientHeight, DXGI_FORMAT_R8G8B8A8_UNORM, 0));
    ID3D11Texture2D* backBuffer;
    HR_DEBUG(swapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), reinterpret_cast<void**>(&backBuffer)));
    HR_DEBUG(d3dDevice->CreateRenderTargetView(backBuffer, nullptr, &renderTargetView));
    ReleaseCOM(backBuffer);

    // Create depth/stencil
    D3D11_TEXTURE2D_DESC depthStencilDesc;

    depthStencilDesc.Width = clientWidth;
    depthStencilDesc.Height = clientHeight;
    depthStencilDesc.MipLevels = 1;
    depthStencilDesc.ArraySize = 1;
    depthStencilDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
    depthStencilDesc.SampleDesc.Count = 1;      // No 4X MSAA yet
    depthStencilDesc.SampleDesc.Quality = 0;    // No 4X MSAA yet
    depthStencilDesc.Usage = D3D11_USAGE_DEFAULT;
    depthStencilDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
    depthStencilDesc.CPUAccessFlags = 0;
    depthStencilDesc.MiscFlags = 0;

    HR_DEBUG(d3dDevice->CreateTexture2D(&depthStencilDesc, nullptr, &depthStencilBuffer));
    HR_DEBUG(d3dDevice->CreateDepthStencilView(depthStencilBuffer, nullptr, &depthStencilView));

    // Bind to the pipeline
    d3dImmediateContext->OMSetRenderTargets(1, &renderTargetView, depthStencilView);

    // Set the viewport
    screenViewport.TopLeftX = 0;
    screenViewport.TopLeftY = 0;
    screenViewport.Width = static_cast<float>(clientWidth);
    screenViewport.Height = static_cast<float>(clientHeight);
    screenViewport.MinDepth = 0.0f;
    screenViewport.MaxDepth = 1.0f;
    d3dImmediateContext->RSSetViewports(1, &screenViewport);

    // Restore the GameViews
    if (gameWorld)
    {
        for (const auto& view : gameWorld->GetGameViews())
        {
            view->OnRestore();
        }
    }
}

LRESULT BaseApplication::InternalMsgProc(HWND hwnd, int msg, WPARAM wParam, LPARAM lParam)
{
    BaseApplication* tempApp = reinterpret_cast<BaseApplication*>(GetWindowLongPtr(hwnd, GWLP_USERDATA));

    if (tempApp == nullptr)
        return DefWindowProc(hwnd, msg, wParam, lParam);

    return tempApp->MsgProc(hwnd, msg, wParam, lParam);
}
