#pragma once

#include <Windows.h>

// TODO: Figure out a better place for this
struct AppMsg
{
    HWND hwnd;
    UINT msg;
    WPARAM wParam;
    LPARAM lParam;
};

///////////////////////////////////
// Usings
///////////////////////////////////
using ActorId = unsigned int;
const ActorId INVALID_ACTOR_ID = 0xffffffff;;

///////////////////////////////////
// Common Macros and Functions/
///////////////////////////////////

// Safely delete objects
#define SafeDelete(x) { delete x; x = nullptr; }

// Release COM objects safely
#define ReleaseCOM(x) { if (x) { x->Release(); x = nullptr; } }

// Simple HRESULT Error Checker
// TODO: Fix Bug in this MessageBox error reporting.
#if defined(DEBUG) | defined(_DEBUG)
    #ifndef HR_DEBUG                                                    
        #define HR_DEBUG(x)                                                                     \
                {                                                                               \
                    HRESULT hr = (x);                                                           \
                    if (FAILED(hr))                                                             \
                    {                                                                           \
                        LPWSTR output;                                                          \
                        FormatMessageW(FORMAT_MESSAGE_FROM_SYSTEM |                             \
                                      FORMAT_MESSAGE_IGNORE_INSERTS |                           \
                                      FORMAT_MESSAGE_ALLOCATE_BUFFER,                           \
                                      nullptr,                                                  \
                                      hr,                                                       \
                                      LANG_SYSTEM_DEFAULT,                                      \
                                      (LPWSTR)&output,                                          \
                                      0,                                                        \
                                      nullptr);                                                 \
                                                                                                \
                        MessageBoxW(nullptr, output, L"Error", MB_OK);                          \
                    }                                                                           \
                }
    #endif
#else
    #ifndef HR_DEBUG
        #define HR_DEBUG(x) (x)
    #endif
#endif