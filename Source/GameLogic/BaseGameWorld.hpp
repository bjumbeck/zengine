#pragma once

#include <GameLogic/GameLogicInterfaces.hpp>

enum class GameState : int
{
    Invalid,
    Initializing,
    Running,
};

class BaseGameWorld
{
    public:
        BaseGameWorld();
        virtual ~BaseGameWorld();

        // Main Functionality
        virtual bool LoadGameWorld(const char* levelResource = nullptr);
        virtual void OnUpdate(float deltaTime);

        virtual void ChangeState(GameState newState);
        const GameState GetState() const { return gameState; }

        // GameViews Related
        virtual std::list<std::shared_ptr<IGameView>> GetGameViews() const { return gameViews; }
        virtual void AddView(std::shared_ptr<IGameView> view);
        virtual void RemoveView(std::shared_ptr<IGameView> view);

    protected:
        float gameLifetime;
        GameState gameState;

        // GameView Related
        int numHumanPlayers;
        int numAiPlayers;
        std::list<std::shared_ptr<IGameView>> gameViews;
};