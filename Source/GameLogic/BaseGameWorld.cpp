#include <GameLogic/BaseGameWorld.hpp>

BaseGameWorld::BaseGameWorld()
    : gameLifetime(0.0f)
    , gameState(GameState::Initializing)
    , numHumanPlayers(0)
    , numAiPlayers(0) 
{
    
}

BaseGameWorld::~BaseGameWorld()
{
    while (!gameViews.empty())
        gameViews.pop_front();
}

bool BaseGameWorld::LoadGameWorld(const char* levelResource)
{
    return true;
}

void BaseGameWorld::OnUpdate(float deltaTime)
{
    gameLifetime += deltaTime;

    switch (gameState)
    {
        case GameState::Invalid: 
            MessageBox(nullptr, L"Invalid game state!", L"Warning", 0);
            break;

        case GameState::Initializing: 
            // If we are here we have already initialized
            ChangeState(GameState::Running);
            break;

        case GameState::Running:
            break;

        default: 
            break;
    }

    // Update game views
    for (const auto& view : gameViews)
    {
        view->OnUpdate(deltaTime);
    }
}

void BaseGameWorld::ChangeState(GameState newState)
{
    // Handle special logic here

    gameState = newState;
}

void BaseGameWorld::AddView(std::shared_ptr<IGameView> view)
{
    int viewId = static_cast<int>(gameViews.size());
    gameViews.push_back(view);
    view->OnAttach(viewId, INVALID_ACTOR_ID); // No actor attachment supported yet, though will add in the future
    view->OnRestore(); // Initializes the view
}

void BaseGameWorld::RemoveView(std::shared_ptr<IGameView> view)
{
    gameViews.remove(view);
}
