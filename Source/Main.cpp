#include <Application/BaseApplication.hpp>

#pragma comment (lib, "d3d11.lib")
#pragma comment (lib, "Winmm.lib") // timeGetTime()

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE prevInstance, PSTR cmdLine, int showCmd)
{
    #if defined(DEBUG) | defined(_DEBUG)
        _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
    #endif

    BaseApplication mainApp;

    if (FAILED(mainApp.InitializeWin32(hInstance)))
        return 0;

    if (FAILED(mainApp.InitializeD3D()))
        return 0;

    return mainApp.RunApp();
}