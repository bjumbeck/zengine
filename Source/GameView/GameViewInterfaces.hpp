#pragma once

#include <Windows.h>
#include <cstdint>
#include <ProjectGlobals.hpp>
#include <memory>
#include <DirectXColors.h>

using GameViewId = unsigned int;
const GameViewId INVALID_GAME_VIEW_ID = 0xffffffff;
const unsigned int SCREEN_REFRESH_RATE_MS = 1000 / 60;

enum class GameViewType : uint_fast8_t
{
    GameViewHuman = 0,
    GameViewAI = 1,
    GameViewRemote = 2,
    GameViewRecorder = 3,
    GameViewOther = 4,
};

class IGameView
{
    public:
        virtual ~IGameView() {}

        virtual void OnAttach(GameViewId viewId, ActorId actorId) = 0;
        virtual HRESULT OnRestore() = 0;
        virtual HRESULT OnLostDevice() = 0;

        // Main Functionality
        virtual void OnUpdate(float deltaTime) = 0;
        virtual void OnRender(float deltaTime) = 0;
        virtual LRESULT CALLBACK OnMsgProc(AppMsg msg) = 0;

        // Accessors, overload these to define certain traits of the view
        virtual GameViewType GetViewType() = 0;
        virtual GameViewId GetId() const = 0;
};

// This is used to represnt all things that the GameView must draw.
// Some examples would be the game World and a screen element,
// User Interface controls grouped together, etc.
class IScreenElement
{
    public:
        virtual ~IScreenElement() {}

        virtual const bool operator<(const IScreenElement& other) { return GetZOrder() < other.GetZOrder(); }

        virtual HRESULT OnRestore() = 0;
        virtual HRESULT OnLostDevice() = 0;

        virtual void OnUpdate(float deltaTime) = 0;
        virtual void OnRender(float deltaTime) = 0;
        virtual LRESULT CALLBACK OnMsgProc(AppMsg msg) = 0;

        virtual int GetZOrder() const = 0;
        virtual void SetZOrder(const int zOrder) = 0;
        virtual bool IsVisible() const = 0;
        virtual void SetVisible(bool visible) = 0;
};

class IRenderState
{

};

class IRenderer
{
    public:
    virtual void Shutdown() = 0;
    virtual HRESULT OnRestore() = 0;

    // Render
    virtual bool PreRender() = 0;
    virtual bool PostRender() = 0;
    virtual std::shared_ptr<IRenderState> PrepareAlphaPass() = 0;
    virtual std::shared_ptr<IRenderState> PrepareSkyBoxPass() = 0;

    virtual void DrawLine(const DirectX::XMVECTOR& start, const DirectX::XMVECTOR& end, const DirectX::XMVECTOR& color) = 0;

    // Lighting
    virtual void CalculateLighting(class Lights* lights, int maximumLights) = 0;

    // Setters
    virtual void SetBackgroundColor(BYTE red, BYTE green, BYTE blue, BYTE alpha) = 0;
    virtual void SetWorldTransform(const DirectX::XMMATRIX* matrix) = 0;
    virtual void SetViewTransform(const DirectX::XMMATRIX* matrix) = 0;
    virtual void SetProjectionTransform(const DirectX::XMMATRIX* matrix) = 0;
};