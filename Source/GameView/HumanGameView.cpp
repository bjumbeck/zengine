#include "HumanGameView.hpp"
#include <Application/BaseApplication.hpp>

HumanGameView::HumanGameView(std::shared_ptr<IRenderer> renderer)
    : actorId(INVALID_ACTOR_ID)
    , currentTime(0)
    , lastDrawTime(0)
    , runFullSpeed(false)
{
    // TODO: Initialize Audio

    // TODO: Initialize Process Manager 

    viewId = INVALID_GAME_VIEW_ID;

    if (renderer)
    {
        // TODO: Declare the Frustum, Camera, Scene.
    }
}

HumanGameView::~HumanGameView()
{
    while (!screenElements.empty())
    {
        screenElements.pop_front();
    }

    // TODO: Safe Delete Process Manager and Audio
}

void HumanGameView::TogglePause(bool paused)
{
    if (paused)
    {
        // TODO: Pause Audio
    }
    else
    {
        // Resume Audio
    }
}

void HumanGameView::OnAttach(GameViewId viewId, ActorId actorId)
{
    this->viewId = viewId;
    this->actorId = actorId;
}

HRESULT HumanGameView::OnRestore()
{
    HRESULT hr;

    for (auto& element : screenElements)
    {
        hr = element->OnRestore();
        if (FAILED(hr))
        {
            return hr;
        }
    }

    return S_OK;
}

HRESULT HumanGameView::OnLostDevice()
{
    HRESULT hr;

    for (auto& element : screenElements)
    {
        hr = element->OnLostDevice();
        if (FAILED(hr))
        {
            return hr;
        }
    }

    return S_OK;
}

void HumanGameView::OnUpdate(float deltaTime)
{
    // TODO: Update process manager

    for (auto& element : screenElements)
    {
        element->OnUpdate(deltaTime);
    }
}

void HumanGameView::OnRender(float deltaTime)
{
    // Early out we already called render this frame
    currentTime = g_Application->GetGameTimer().TotalTime();
    if (currentTime == lastDrawTime)
        return;

    if (runFullSpeed || currentTime - lastDrawTime > SCREEN_REFRESH_RATE_MS)
    {
        IRenderer* renderer = g_Application->GetRenderer();
        if (renderer)
        {
            if (renderer->PreRender())
            {
                screenElements.sort([](const std::shared_ptr<IScreenElement>& lhs, const std::shared_ptr<IScreenElement>& rhs)
                {
                    return *lhs < *rhs;
                });

                for (const auto& element : screenElements)
                {
                    if (element->IsVisible())
                    {
                        element->OnRender(deltaTime);
                    }
                }

                // Render any text here

                lastDrawTime = currentTime;
            }

            renderer->PostRender();
        }
        else
        {
            MessageBox(nullptr, L"Renderer is null in g_Application.", L"Error", 0);
        }
    }
}

LRESULT HumanGameView::OnMsgProc(AppMsg msg)
{
    // Send input to scren layers first
    // The end of the list is the topmost layer on the screen
    for (auto iter = screenElements.rbegin(); iter != screenElements.rend(); ++iter)
    {
        if ((*iter)->IsVisible())
        {
            // Returns true if that layer consumed the message
            if ((*iter)->OnMsgProc(msg))
            {
                return 1;
            }
        }
    }

    // If the screen layers didn't consume the message move onto the input handlers
    LRESULT result = 0;
    switch (msg.msg)
    {
        case WM_KEYDOWN:
            // TODO: Keyboard stuff
            break;

        case WM_KEYUP:
            // TODO: Keyboard stuff
            break;

        case WM_MOUSEMOVE:
            // TODO: Mouse Stuff
            break;

        case WM_LBUTTONDOWN:
            // TODO: Mouse Stuff
            break;

        case WM_LBUTTONUP:
            // TODO: Mouse Stuff
            break;

        case WM_RBUTTONDOWN:
            // TODO: Mouse Stuff
            break;

        case WM_RBUTTONUP:
            // TODO: Mouse Stuff
            break;
    }

    return result;
}

void HumanGameView::PushScreenElement(std::shared_ptr<IScreenElement> element)
{
    screenElements.push_front(element);
}

void HumanGameView::RemoveScreenElement(std::shared_ptr<IScreenElement> element)
{
    screenElements.remove(element);
}
