#pragma once

#include <GameView/GameViewInterfaces.hpp>
#include <memory>
#include <list>

class IRenderer;

class HumanGameView : public IGameView
{
    public:
        HumanGameView(std::shared_ptr<IRenderer> renderer);
        virtual ~HumanGameView();

        void TogglePause(bool paused);

        // IGameView Interface
        virtual void OnAttach(GameViewId viewId, ActorId actorId) override;
        virtual HRESULT OnRestore() override;
        virtual HRESULT OnLostDevice() override;

        virtual void OnUpdate(float deltaTime) override;
        virtual void OnRender(float deltaTime) override;
        virtual LRESULT CALLBACK OnMsgProc(AppMsg msg) override;

        virtual void PushScreenElement(std::shared_ptr<IScreenElement> element);
        virtual void RemoveScreenElement(std::shared_ptr<IScreenElement> element);

        virtual GameViewType GetViewType() override { return GameViewType::GameViewHuman; }
        virtual GameViewId GetId() const override { return viewId; }

    protected:
        GameViewId viewId;
        ActorId actorId; // The actor this view is "following" if any (Think camera in 3rd person shooter)

        float currentTime;
        float lastDrawTime;
        bool runFullSpeed;

        std::list<std::shared_ptr<IScreenElement>> screenElements;
};
