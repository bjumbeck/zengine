Game View Layer
==================

The game view layer is a collection of systems that communicate with the game logic layer to
present the game to a particular kind of observer. This observer can be a human with a controller, monitor and mouse
or it could be an AI agent.

The game view layer's responsibiliy is as follows.

## Human View

* Handle visual output.
	* Game Scene
	* User Interface
* Handle audio output.
	* SFX
	* Music
	* Speech
* Interpret input from devices into commands that the logic layer understands.
* User based options (Things like sound volume, resolution, etc.)

## AI view

* Recieves events of actions happening in the game and simulates input in response to those events.
* Decision makings
* AI based options